$(function () {

    console.log($('.menu_slider'));


    $('#navbar ul.main-nav li.menu_item_main').hover(function () {

        $(this).find('.menu_slider')
            .not('.slick-initialized')
            .slick({
                dots: true,
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                variableWidth: true
            });
    });


    var slideout = new Slideout({
        'panel': document.getElementById('panel'),
        'menu': document.getElementById('menu'),
        'padding': 256,
        'tolerance': 70
    });

    document.querySelector('.toggle-button').addEventListener('click', function () {
        slideout.toggle();
    });


    $('.slider-carousel').slick({
        dots: true,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3
    });


    $('.item_subjects').masonry({
        // options
        itemSelector: '.item_subject',
        columnWidth: 340

    });


    var s = Snap("#svg_principle");
    console.log(s);
    var circle1 = s.circle(500, 180, 120);
    var text1 = s.text(50, 50, "Snap");

    text1.attr({
        size: "25"
    });

    circle1.attr({
        fill: "#cfc1e5",
        stroke: "#000",
        strokeWidth: 0
    });

    var circle2 = s.circle(730, 145, 120);

    circle2.attr({
        fill: "#b5e7fc",
        stroke: "#000",
        strokeWidth: 0
    });

    var circle3 = s.circle(650, 300, 120);

    circle3.attr({
        fill: "#b9e0b9",
        stroke: "#000",
        strokeWidth: 0
    });


    var map;

    DG.then(function () {
        map = DG.map('map2', {
            center: [52.250978, 104.31407],
            zoom: 13

        });

        icon = new DG.Icon({
            iconUrl: 'http://e-a-s-y.ru/wp-content/uploads/2017/08/espng.png'
        });


        DG.marker([52.2809393, 104.3050183], {
            icon: icon
        }).addTo(map)

        DG.marker([52.280081, 104.3324], {
            icon: icon
        }).addTo(map)

        DG.marker([52.2526843, 104.3652923], {
            icon: icon
        }).addTo(map)

        DG.marker([52.2582535, 104.3222299], {
            icon: icon
        }).addTo(map)

        DG.marker([52.2349039, 104.2772798], {
            icon: icon
        }).addTo(map)

        DG.marker([52.2630176, 104.2593192], {
            icon: icon
        }).addTo(map)


    });

});

//
// function myMap() {
//
//     var locations = [
//         ['ES', 52.2809393, 104.3050183, 4],
//         ['ES', 52.280081, 104.3324, 5],
//         ['ES', 52.2526843, 104.3652923, 3],
//         ['ES', 52.2582535, 104.3222299, 2],
//         ['ES', 52.2349039, 104.2772798, 1],
//         ['ES', 52.2630176, 104.2593192, 6]
//     ];
//
//     var map = new google.maps.Map(document.getElementById('map'), {
//         zoom: 12,
//         center: new google.maps.LatLng(52.2809393, 104.3050183),
//         mapTypeId: google.maps.MapTypeId.ROADMAP
//     });
//
//     var infowindow = new google.maps.InfoWindow();
//
//     var marker, i;
//
//     for (i = 0; i < locations.length; i++) {
//         marker = new google.maps.Marker({
//             position: new google.maps.LatLng(locations[i][1], locations[i][2]),
//             map: map,
//             icon: "http://e-a-s-y.ru/wp-content/uploads/2017/08/espng.png"
//         });
//
//         google.maps.event.addListener(marker, 'click', (function (marker, i) {
//             return function () {
//                 infowindow.setContent(locations[i][0]);
//                 infowindow.open(map, marker);
//             }
//         })(marker, i));
//     }
// }



